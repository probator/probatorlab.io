FROM python:3.7-alpine

ENV PATH="/venv/bin:${PATH}"

RUN apk --no-cache add \
    curl \
    gcc \
    git \
    jq \
    libc-dev \
    libffi-dev \
    make \
    mariadb-dev \
    tzdata

RUN python3.7 -m venv /venv
RUN pip install --no-cache-dir -U \
    probator \
    probator-auditor-cloudtrail \
    probator-auditor-domain-hijacking \
    probator-auditor-iam \
    probator-auditor-required-tags \
    probator-auditor-vpc-flowlogs \
    probator-collector-dns \
    probator-scheduler-sqs \
    pip \
    sphinx \
    sphinx-autodoc-typehints \
    guzzle_sphinx_theme

RUN mkdir -p /usr/local/etc/probator
COPY files/* /usr/local/etc/probator/
