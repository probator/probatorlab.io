Built-in plugins
================

.. toctree::
    :maxdepth: 1

    auth/builtin
    collectors/base
    commands/base
    notifiers/base
    types/base

Optional plugins
================
.. toctree::
    :maxdepth: 1

    auditors/optional
    collectors/base
    commands/base
    notifiers/base
    types/base

Module contents
---------------

.. automodule:: probator.plugins
    :members:
    :no-undoc-members:
    :show-inheritance:
