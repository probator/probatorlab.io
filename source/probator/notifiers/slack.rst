probator.plugins.notifiers.slack
================================

.. automodule:: probator.plugins.notifiers.slack
    :members:
    :no-undoc-members:
    :show-inheritance:
