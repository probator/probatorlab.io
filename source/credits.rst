Credits
=======

Main Author
-----------

- Asbjorn Kjaer

Contributors
------------

- Mark Hillick
- Marty Chong
- Gabe Friedmann
- Dong Liu

Special Thanks
--------------

A special thanks to the Security team at Riot Games. Probator is forked from CloudInquisitor_, built and maintained
by the team at Riot Games.

.. _CloudInquisitor: https://github.com/RiotGames/cloud-inquisitor/
