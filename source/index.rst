Probator
========

Probator is a highly customizable, pluggable framework used for scanning and auditing your cloud environments for security and compliance
issues, as well as providing a single pane of glass for search for the supported resource types.

It's fairly easy to implement plugins on your own, in order to extend the functionality, without having to have the plugins be added to the
official codebase, allowing you to develop plugins without providing code to anyone outside your organization if necessary.

If this is the first time you are looking at using Probator, you should go directly to the `User Guide`_, which will get you started with
building an AMI and deploying it to your AWS infrastructure.

.. toctree::
    :maxdepth: 2

    quickstart
    userguide
    docs/index
    api_reference
    credits


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _User Guide: userguide.html
