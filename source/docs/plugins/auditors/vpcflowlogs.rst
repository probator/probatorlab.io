VPCFlowLogsAuditor
==================

The VPC Flow Logs Auditor will ensure that all VPCs in all of your managed AWS accounts will be configured to enable VPC Flow Logging. You
can either deliver the logs to a central S3 bucket, or you can have the system create a CloudWatch Log Group for each VPC and have the logs
delivered there.

**NOTE** If using the S3 bucket delivery method, you must create the bucket manually and grant the proper permissions on the bucket to
enable log file delivery. More information on setting the correct permissions can be found in the official `Flow Log Documentation`_

Config Options
--------------

+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| Option name           | Default Value                 | Type          | Description                                               |
+=======================+===============================+===============+===========================================================+
| ``enabled``           | ``False``                     | ``bool``      | Enable the EBS auditor                                    |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``interval``          | ``60``                        | ``int``       | Run frequency in minutes                                  |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``role_name``         | ``VpcFlowLogsRole``           | ``string``    | Name of the role created to grant access to the           |
|                       |                               |               | CloudWatch Log Group created to the VPC Flow Logs system  |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``delivery_method``   | ``s3``                        | ``choice``    | Delivery method, either ``s3`` or ``cloudwatch-logs``     |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``traffic_type``      | ``ALL``                       | ``choice``    | Type of traffic to log, one of ``ALL``, ``ACCEPT`` or     |
|                       |                               |               | ``REJECT``                                                |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``bucket_name``       | *None*                        | ``string``    | Name of the bucket to deliver logs to                     |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+

.. _Flow Log Documentation: https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs-s3.html
