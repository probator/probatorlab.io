CloudTrailAuditor
=================

The CloudTrail auditor will ensure that all of your AWS Accounts have CloudTrail enabled and configured to send all of your log files to a
single central location, for processing (auditor does not process log files).

Config Options
--------------

The table below describes all the configuration options for the CloudTrail auditor

+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| Option name           | Default Value | Type          | Description                                                               |
+=======================+===============+===============+===========================================================================+
| ``enabled``           | ``False``     | ``bool``      | Enable the CloudTrail auditor                                             |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``interval``          | ``60``        | ``int``       | Run frequency in minutes                                                  |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``bucket_account``    | *None*        | ``string``    | Name of the S3 bucket to send CloudTrail logs to                          |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``bucket_name``       | *None*        | ``string``    | Name of account to create the S3 bucket in                                |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``bucket_region``     | ``us-west-2`` | ``string``    | Region to create S3 bucket in                                             |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``cloudtrail_region`` | ``us-west-2`` | ``string``    | Region to create CloudTrail in                                            |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``sns_topic_name``    | *None*        | ``string``    | SNS topic name for CloudTrail log delivery                                |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``sqs_queue_account`` | *None*        | ``string``    | Name of account of SQS queue for CloudTrail log delivery notifications    |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``sqs_queue_name``    | *None*        | ``string``    | SQS queue name                                                            |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``sqs_queue_region``  | ``us-west-2`` | ``string``    | Region for the SQS queue                                                  |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
| ``trail_name``        | ``us-west-2`` | ``string``    | Name of the trail to create                                               |
+-----------------------+---------------+---------------+---------------------------------------------------------------------------+
