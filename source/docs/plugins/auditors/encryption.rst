EncryptionAuditor
=================

The Encryption Auditor will validate that your resources created in the cloud are using server side encryption / encryption at rest.

The auditor currently supported checking EBS Volumes, EBS Snapshots and RDS Instances for encryption settings.

Config Options
--------------

+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| Option name           | Default Value                 | Type          | Description                                               |
+=======================+===============================+===============+===========================================================+
| ``enabled``           | ``False``                     | ``bool``      | Enable the EBS auditor                                    |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``interval``          | ``60``                        | ``int``       | Run frequency in minutes                                  |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``audited_resources`` | *None*                        | ``choice``    | List of resource types to audit                           |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
