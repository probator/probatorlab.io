IAMAuditor
==========

The IAM Auditor is responsible for ensuring that the roles and policies you configure are always kept in sync across some or all of your
AWS Accounts. When enabled the auditor will use the ``roles`` setting to create and manage roles in every AWS account in the system, as well
as pulling policies and account-specific roles and policies from a Git repository

Roles Configuration
-------------------

The `roles` setting allows you to configure roles to create and manage on all accounts enabled in Probator. The JSON document is structured
as dictionary, with the top-level key being the name of the role, and the dictionary value has two keys; `trust` and `policies`

trust
^^^^^

The ``trust`` setting must be a valid IAM Assume Role Policy Document. If the ``trust`` key is an empty object (``{}``), null, or not set,
the default trust document is provided.

policies
^^^^^^^^

The ``policies`` key contains a list of IAM policy names to attach to the role. These policies must exist within the account before running
the auditor, ideally being created by the auditor as well.

Example configuration
^^^^^^^^^^^^^^^^^^^^^

The example below shows how one can manage the role probator assumes for monitoring and auditing in your accounts

.. code-block:: json

    {
        "probator_role": {
            "trust": {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "",
                        "Effect": "Allow",
                        "Principal": {
                            "AWS": "arn:aws:iam::123456789012:role/probator-instance-role",
                            "Service": "ec2.amazonaws.com"
                        },
                        "Action": "sts:AssumeRole"
                    }
                ]
            },
            "policies": [
                "ProbatorAccess"
            ]
        }
    }

The example above would create a role on all accounts called ``probator_role``, that allows a role from another account
(``arn:aws:iam::123456789012:role/probator-instance-role``) to assume it.

Config Options
--------------

+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| Option name                   | Default Value     | Type          | Description                                                           |
+===============================+===================+===============+=======================================================================+
| ``enabled``                   | ``False``         | ``bool``      | Enable the CloudTrail auditor                                         |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``interval``                  | ``60``            | ``int``       | Run frequency in minutes                                              |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``manage_roles``              | ``True``          | ``bool``      | Enable management of IAM roles                                        |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``roles``                     | ``{}``            | ``json``      | A JSON document describing the global roles (see above)               |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``delete_inline_policies``    | ``False``         | ``bool``      | Delete inline policies, if present, from managed roles                |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``hostname``                  | *None*            | ``string``    | Hostname of the Git server                                            |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``repository``                | *None*            | ``string``    | Path of the Git repository                                            |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``disable_ssl_verify``        | ``False``         | ``bool``      | Disable SSL verification of Git server, if https. Useful if using     |
|                               |                   |               | self-signed certificates                                              |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``authentication_type``       | ``oauth-token``   | ``choice``    | Type of authentication for API requests                               |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``oauth_token``               | *None*            | ``string``    | OAuth2 authentication token, if using ``oauth-token`` authentication  |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``username``                  | *None*            | ``string``    | Username, if using ``username-password`` authentication               |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
| ``password``                  | *None*            | ``string``    | Password, if using ``username-password`` authentication               |
+-------------------------------+-------------------+---------------+-----------------------------------------------------------------------+
