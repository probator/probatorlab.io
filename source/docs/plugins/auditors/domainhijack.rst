DomainHijackAuditor
===================

The Domain Hijacking Auditor helps you prevent domain takeovers by malicious actors, possible due to misconfigurations in your environment.

If you create a DNS record and point it to a cloud resource, such as an S3 Bucket or an EC2 Instance IP, if you delete the resource you are
referencing in your record, anyone else can create the resource in question and are now able to serve content to your users and customers
from a legitimate domain name and trick people into providing for example login information.

The auditor uses information from all provided DNSZone and DNSRecords and checks to see if they point to a known vulnerable service, and if
the do it will ensure that the resource in question is known to the Probator system. Some checks are able to go further and alert if the
resource does exist but is not tracked in any of your accounts, as that may mean it has already been exploited.

Unlike other auditors, which will notify the owners of the accounts, since the DNS and resources could potentially be on seperate accounts
this auditor uses a list of email addresses from a configuration setting instead, and you would include your security team / SOC email
addresses in this list.

Config Options
--------------

+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| Option name           | Default Value                 | Type          | Description                                               |
+=======================+===============================+===============+===========================================================+
| ``enabled``           | ``False``                     | ``bool``      | Enable the DomainHijacking auditor                        |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``interval``          | ``60``                        | ``int``       | Run frequency in minutes                                  |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``email_recipients``  | *None*                        | ``array``     | List of email addresses to receive alerts                 |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``hijack_subject``    | ``Potential hijack detected`` | ``string``    | Subject of the email alert                                |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
