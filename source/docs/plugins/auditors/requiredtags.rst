RequiredTagsAuditor
===================

The Required Tags auditor will alert and optionally enforce tags on your cloud resources. Currently the auditor supports only EC2 Instances,
but more resource types will be added at a later date.

Config Options
--------------

+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| Option name               | Default Value                 | Type          | Description                                               |
+===========================+===============================+===============+===========================================================+
| ``enabled``               | ``False``                     | ``bool``      | Enable the EBS auditor                                    |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``interval``              | ``60``                        | ``int``       | Run frequency in minutes                                  |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``alert_settings``        | (See below)                   | ``json``      | `Alert Settings`_ configuration                           |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``audit_ignore_tag``      | ``probator:ignore``           | ``string``    | Name of tag which, if present, will cause the auditor to  |
|                           |                               |               | ignore the resource                                       |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``audit_scope``           | *None*                        | ``choice``    | Resource types to audit                                   |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``collect_only``          | ``True``                      | ``bool``      | Only collect information, do not enforce                  |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``always_send_emails``    | ``True``                      | ``bool``      | Send emails even in collect only mode                     |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``email_subject``         | ``Resources missing required  | ``string``    | Subject of email alerts                                   |
|                           | tags``                        |               |                                                           |
+---------------------------+-------------------------------+---------------+-----------------------------------------------------------+

Alert Settings
^^^^^^^^^^^^^^

Below you can see the default value for the ``alert_settings`` item. The default setting will send 3 alerts and then shutdown the instance
before finally terminating after a 12 week wait.

The JSON document is a dictionary/map of keys, where the key is the resource type name (ex: ``aws_ec2_instance``). There is a special key
called ``global`` that will be applied to any resource type that does not have an explicit configuration.

If you add a resource type specific section, such as in the example below, if you provide a new ``actions`` list, you need to provide the
entire list of actions, as it will override the global actions. However if you provide the ``requiredTags`` section, it will be merged with
the list of tags from the ``global`` section as well.

Any required tags listed in the ``global`` section will be required on all resources, but the resource type specific tags allows you to add
extra tags for just one or more types of resources.

Actions
+++++++

The actions define what to do at specific intervals. Every action consists of 4 fields;

+---------------+---------------------------------------------------------------------------+
| Name          | Description                                                               |
+===============+===========================================================================+
| ``name``      | Name of the action, shown to end users                                    |
+---------------+---------------------------------------------------------------------------+
| ``age``       | Age of the issue, in a human readable format, required to trigger the     |
|               | state change. See available formats in the `pyparsetime docs`_            |
+---------------+---------------------------------------------------------------------------+
| ``action``    | The action to perform, must be one of ``alert``, ``stop`` or ``remove``.  |
+---------------+---------------------------------------------------------------------------+
| ``order``     | Order from lowest to highest to control the next action to perform. This  |
|               | value is zero-indexed                                                     |
+---------------+---------------------------------------------------------------------------+

Its important to note that not all resource types support being stopped. An example of this could be S3 Buckets, which are either present
or not as you cannot "stop" an S3 Bucket.

Required Tags
+++++++++++++

The ``requiredTags`` section defines what tags are required as a dictionary/map object, where the ``key`` will match the tag's Key. If you
just want to test for the presence the tag you would set the value to ``null``. However if you need to validate the value of the tag you can
provide a regular expression that will be used to match against the tag's value.

In the example below, you can see that the ``Name`` tag does not do any validation, but for the ``owner`` tag it must match the supplied
regular expression, which in this case you match any valid email addresses.

**Note** The regular expression matches are performed across the entire string. If you need to match the entire value you should utilize the
regular expression ``start-of-line`` and ``end-of-line`` markers, ``^`` and ``$`` respectively. For example if you want to enforce the
entire value of the tag to be either ``true`` or ``false`` you would provide the following expression: ``^(true|false)$``

Extra Config
++++++++++++

The ``extraConfig`` section is currently not used, but provided in order to pass extra arguments for the ``stop`` and ``remove`` methods,
and can be any arbitraty valid JSON values. As mentioned the built-in methods do not currently use any ``extraConfig`` values.

Default Value
+++++++++++++

.. code-block:: json

    {
        "global": {
            "actions": [
                {
                    "name": "alert:1",
                    "age": "now",
                    "action": "alert",
                    "order": 0
                },
                {
                    "name": "alert:2",
                    "age": "3 weeks",
                    "action": "alert",
                    "order": 1
                },
                {
                    "name": "alert:3",
                    "age": "3 weeks, 6 days",
                    "action": "alert",
                    "order": 2
                },
                {
                    "name": "stop",
                    "age": "4 weeks",
                    "action": "stop",
                    "order": 3
                },
                {
                    "name": "remove",
                    "age": "12 weeks",
                    "action": "remove",
                    "order": 4
                }
            ],
            "requiredTags": {
                "Name": null,
                "owner": "([a-zA-Z0-9._%+-]+[^+]@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,})"
            },
            "extraConfig": {}
        },
        "aws_ec2_instance": {
            "requiredTags": {
                "ssmEnabled": "^(true|false)$"
            }
        }
    }

.. _pyparsetime docs: https://pypi.org/project/pytimeparse/
