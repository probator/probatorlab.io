EBSAuditor
==========

This auditor will alert you to any EBS Volume that are created but not attached to any resources. Large amounts of EBS volumes, especially
if they use provisioned IOPS, can end up costing a lot of money, and it can be difficult to know the content of the volumes. If the volumes
contain PII, HIPAA or similarly sensitive data, it can be a compliance risk having the volumes stay around.

It is possible to mark a volume to be ignored by the auditor, if there are legitimate reasons for having the volumes persist unattached.

Config Options
--------------

+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| Option name           | Default Value                 | Type          | Description                                               |
+=======================+===============================+===============+===========================================================+
| ``enabled``           | ``False``                     | ``bool``      | Enable the EBS auditor                                    |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``interval``          | ``60``                        | ``int``       | Run frequency in minutes                                  |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``ignore_tags``       | ``["probator:ignore"]``       | ``array``     | List of tags that, if present, will cause the auditor to  |
|                       |                               |               | ignore the volume                                         |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``email_subject``     | ``Unattached EBS Volumes``    | ``string``    | Subject of the email alert                                |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
