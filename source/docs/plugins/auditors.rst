Auditor Plugins
===============

Auditor plugins are plugins that do not collect any information from remote API's, but operate on the information collected by the
`collectors`_. Typical auditor actions include;

* **Perform changes on resources directly** - The CloudTrail auditor will automatically create and maintain your CloudTrail settings
* **Create issues for operators to handle** - The Domain Hijacking Auditor simply raises issues, and operators are responsible for remidiating the issue
* **Both** - The Required Tags Auditor will create issues and notify operators and account owners, but will also automatically stop instances that are not compliant (if enabled in configuration)

Official Auditors
-----------------

Probator provides a set of officially supported Auditors, that can be installed on top of the core framework.

Below you'll see a table describing each plugin as well as the name of the package to use when installing the plugins with ``pip3 install $PACKAGE``

+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+
| Auditor Name                      | Description                                                           | Package Name                          |
+===================================+=======================================================================+=======================================+
| `CloudTrailAuditor`_              | Audits and maintains CloudTrail logging configuration                 | ``probator_auditor_cloudtrail``       |
+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+
| `DomainHijackAuditor`_            | Checks your environment for any references to Cloud Objects that has  | ``probator_auditor_domain_hijacking`` |
|                                   | been deleted, leaving you vulnerable to a domain hijacking            |                                       |
+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+
| `EBSAuditor`_                     | Alerts if you have EBS volumes that are not attached to instances     | ``probator_auditor_ebs``              |
+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+
| `EncryptionAuditor`_              | Audits your resources to check if the data is encrypted at rest       | ``probator_auditor_encryption``       |
+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+
| `IAMAuditor`_                     | Create and manage IAM roles and Policies. Loads roles and policies    | ``probator_auditor_iam``              |
|                                   | from a git repository                                                 |                                       |
+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+
| `RequiredTagsAuditor`_            | Ensures that resources have the tags required by the configuration.   | ``probator_auditor_required_tags``    |
|                                   | Resources that are non-compliant can be stopped (if applicable) and   |                                       |
|                                   | terminated by the auditor after the operator configured grace periods |                                       |
+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+
| `VPCFlowLogsAuditor`_             | Create and manage VPC Flow Logging setting for all your VPCs, to send | ``probator_auditor_vpc_flowlogs``     |
|                                   | flow logs to a configured central destination                         |                                       |
+-----------------------------------+-----------------------------------------------------------------------+---------------------------------------+


.. toctree::
   :hidden:

   auditors/cloudtrail
   auditors/domainhijack
   auditors/ebs
   auditors/encryption
   auditors/iam
   auditors/requiredtags
   auditors/vpcflowlogs


.. _collectors: collectors.html
.. _CloudTrailAuditor: auditors/cloudtrail.html
.. _DomainHijackAuditor: auditors/domainhijack.html
.. _EBSAuditor: auditors/ebs.html
.. _EncryptionAuditor: auditors/encryption.html
.. _IAMAuditor: auditors/iam.html
.. _RequiredTagsAuditor: auditors/requiredtags.html
.. _VPCFlowLogsAuditor: auditors/vpcflowlogs.html
