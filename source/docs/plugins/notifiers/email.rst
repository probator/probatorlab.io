Email Notifier
==============

The email notifier simply sends emails to users as either plain text, html or both. The system is able to send using either SES (preferred)
or through a standard SMTP servers, with or without authentication.

Config Options
--------------

+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| Option name           | Default Value                 | Type          | Description                                               |
+=======================+===============================+===============+===========================================================+
| ``enabled``           | ``False``                     | ``bool``      | Enable the email notifier                                 |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``from_address``      | *None*                        | ``string``    | EMail address to use as the sender for all emails         |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``method``            | ``ses``                       | ``string``    | Method for sending emails, either ``ses`` or ``smtp``     |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``from_arn``          | *None*                        | ``string``    | Used if using cross-account SES sending. See below        |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``return_path_arn``   | *None*                        | ``string``    | Used if using cross-account SES sending. See below        |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``source_arn``        | *None*                        | ``string``    | Used if using cross-account SES sending. See below        |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``ses_region``        | ``us-west-2``                 | ``string``    | Name of the region to send emails from                    |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``smtp_server``       | ``localhost``                 | ``string``    | Hostname / IP of the SMTP server, if applicable           |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``smtp_port``         | ``25``                        | ``int``       | SMTP Server port, if applicable                           |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``smtp_username``     | *None*                        | ``string``    | SMTP Authentication username, if applicable               |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``smtp_password``     | *None*                        | ``string``    | SMTP Authentication password, if applicable               |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``smtp_tls``          | ``False``                     | ``bool``      | Use TLS encryption for SMTP traffic                       |
+-----------------------+-------------------------------+---------------+-----------------------------------------------------------+

Cross Account SES Access
^^^^^^^^^^^^^^^^^^^^^^^^

If you want to use a single account to send all emails when using the SES method, you will need to supply a couple extra configuration
options; ``from_arn``, ``return_path_arn`` and ``source_arn``. More information on what these values are and how you can configure cross
account SES access, can be found in the official `Amazon SES Documentation`_

.. _Amazon SES Documentation: https://docs.aws.amazon.com/ses/latest/DeveloperGuide/sending-authorization.html
