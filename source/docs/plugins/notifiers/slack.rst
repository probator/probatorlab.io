Slack Notifier
==============

This plugin allows you to deliver messages over Slack, either to channels or directly as private messages to individual users.

Config Options
--------------

+---------------+-------------------------------+---------------+-----------------------------------------------------------+
| Option name   | Default Value                 | Type          | Description                                               |
+===============+===============================+===============+===========================================================+
| ``enabled``   | ``False``                     | ``bool``      | Enable the email notifier                                 |
+---------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``api_key``   | *None*                        | ``string``    | API Key / token for the bot. See below for more info      |
+---------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``bot_name``  | ``Probator``                  | ``string``    | Name of the bot, when sending messages                    |
+---------------+-------------------------------+---------------+-----------------------------------------------------------+
| ``bot_color`` | ``#607d8b``                   | ``string``    | Color (in hex format) for the bot messages to be          |
|               |                               |               | highlighted with                                          |
+---------------+-------------------------------+---------------+-----------------------------------------------------------+

Slack Integration Setup
^^^^^^^^^^^^^^^^^^^^^^^

The token required for the Slack integration to work can be set up by going to Slack's ``Custom Integrations`` page and adding a new
integration in the ``Bots`` section. When creating the integration you will be provided with the token that you need to put into the
``api_key`` configuration setting.
