Notifier Plugins
================

Notifier plugins are responsible for alerting users of the system as needed. Probator ships with two supported plugins out of the box;

+-----------+-----------------------------------------------------------+
| Name      | Description                                               |
+===========+===========================================================+
| `Email`_  | Can send both text and HTML emails to users               |
+-----------+-----------------------------------------------------------+
| `Slack`_  | Sends messages to either Slack channels or users directly |
+-----------+-----------------------------------------------------------+

.. toctree::
    :hidden:

    notifiers/email
    notifiers/slack

.. _Email: notifiers/email.html
.. _Slack: notifies/slack.html
