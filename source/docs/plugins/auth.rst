Authentication Plugins
======================

Authentication plugins are used to authenticate users logging into the web frontend. Probator ships with a plugin utilizing a built-in user
database, but its rather trivial to implement authentication plugins for OIDC, SAML etc.

All authentication plugins must inherit from the ``probator.plugins:BaseAuthPlugin`` class, described below.

Base Auth Plugin
----------------

The ``BaseAuthPlugin`` is itself a subclass of the ``BasePlugin``. In order the successfully implement a base plugin you will need to
implement the following attributes and methods on your class;

+-------------------+-------------------+-----------------------------------------------------------------------------------+
| Name              | Type              | Description                                                                       |
+===================+===================+===================================================================================+
| ``ns``            | attr: ``string``  | The configuration namespace for the plugin                                        |
+-------------------+-------------------+-----------------------------------------------------------------------------------+
| ``views``         | attr: ``list``    | A list of the views needed to handle authentication, such views accepting the     |
|                   |                   | post data from a login form or a callback URL for use with SAML or OIDC           |
+-------------------+-------------------+-----------------------------------------------------------------------------------+
| ``readonly``      | attr: ``bool``    | If set to ``True``, any write operations done from the admin section in the web   |
|                   |                   | frontend will be blocked, except modifying the users roles                        |
+-------------------+-------------------+-----------------------------------------------------------------------------------+
| ``login``         | attr: ``string``  | This attribute provides the frontend information about  where to send the user    |
|                   | or ``dict``       | for login.                                                                        |
|                   |                   |                                                                                   |
|                   |                   | If this is a string, it will be treated as a URL to redirect the user to.         |
|                   |                   |                                                                                   |
|                   |                   | This can also be a single-key ``dict``, where the key is ``state`` and the value  |
|                   |                   | if the name of the route in the ``state`` and the value if the name of the route  |
|                   |                   | in the frontend code to load the user to.                                         |
+-------------------+-------------------+-----------------------------------------------------------------------------------+
| ``bootstrap``     | method            | (Optional) This method will be executed when the API server starts up. This can   |
|                   |                   | be useful to do any initial setup tasks. For example, the built-in local auth     |
|                   |                   | plugin will generate the initial admin user and password, if not present          |
+-------------------+-------------------+-----------------------------------------------------------------------------------+

For a look at a real auth plugin, you can browse the code for the `local authentication`_ plugin in Gitlab

.. _local authentication: https://gitlab.com/probator/probator/blob/master/probator/plugins/auth/local.py
