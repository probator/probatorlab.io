.. _quickstart:

Quick Start Guide
=================

This tutorial will walk you through installing and configuring ``Probator``. The tool currently runs on *Amazon Web Services* (AWS) but it has been designed to be platform independent.

This tutorial assumes you are familiar with AWS & that you have an `AWS`_ account. Its recommended to use IAM Instance Roles for granting access to the system, but if needed you can also configure it with a static API Access and Secret Key pair if needed.

It's highly recommended you first use this quickstart guide to build a Probator AMI. However if you want to explore additional options please see `additional options`_.


Building an AMI
---------------

The recommended way to run Probator is to build an AMI (Amazon Machine Image), and then launch an EC2 Instance from that image. The steps required for this guide are detailed below

1. Requirements
^^^^^^^^^^^^^^^

* `Packer`_ > v1.1

* `AWS Credentials`_ - API Keys or an AWS instance role with `appropriate permissions`_.

* A pre-existing VPC and subnet for the packer build instance and eventually the Probator instance will run

2. Setting up IAM Roles and Policies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Probator is designed to run from a single AWS account and remote access other AWS Accounts using `STS AssumeRole`_.

To ensure this is possible, you will need to create an ``Instance Profile`` so it can use ``AssumeRole`` in the target accounts it is auditing. Below is a sample of the instance profile you should create:

* Create an `IAM policy`_ (within the AWS Console) as follows ::

    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "ProbatorInstancePolicy",
                "Effect": "Allow",
                "Action": [
                    "ec2:DescribeTags",
                    "ses:SendEmail",
                    "ses:SendRawEmail",
                    "sqs:DeleteMessage*",
                    "sqs:GetQueue*",
                    "sqs:ListQueues",
                    "sqs:PurgeQueue",
                    "sqs:ReceiveMessage",
                    "sqs:SendMessage*",
                    "sts:AssumeRole"
                 ],
                "Resource": [
                    "*"
                ]
            }
        ]
    }

If you are using any third-party plugins, you may need to add more permissions to your instance profile.

* Create an `IAM Role`_ (within the AWS Console) and bind the above policy (that you have just created) to it

* For each account the system is auditing you will need to setup an IAM role with the required permissions and with a trust policy document trust policy for *each* target account. This also includes the account that Probator is running from:

On the target account, create an IAM role called ``probator_role`` and attach the `AWS Managed ReadOnlyAccess policy`_ along with the following custom policy: ::

    {
        "Statement": [
            {
                "Sid": "",
                "Effect": "Allow",
                "Resource": [
                    "*"
                ],
                "Action": [
                    "cloudtrail:*",
                    "ec2:CreateFlowLogs",
                    "ec2:CreateTags",
                    "ec2:DeleteTags",
                    "ec2:DeleteVolume",
                    "ec2:StopInstances",
                    "ec2:TerminateInstances",
                    "iam:AttachRolePolicy",
                    "iam:CreatePolicy*",
                    "iam:CreateRole",
                    "iam:DeletePolicy*",
                    "iam:DeleteRolePolicy",
                    "iam:DetachRolePolicy",
                    "iam:PassRole",
                    "iam:PutRolePolicy",
                    "iam:SetDefaultPolicyVersion",
                    "iam:TagRole",
                    "iam:UpdateAssumeRolePolicy",
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:DescribeLogGroups",
                    "logs:DescribeLogStreams",
                    "logs:PutLogEvents",
                    "s3:CreateBucket",
                    "s3:PutBucketPolicy",
                    "sns:CreateTopic",
                    "sns:SetTopicAttributes",
                    "sns:Subscribe",
                    "sqs:Get*",
                    "sqs:Get*",
                    "sqs:List*",
                    "sqs:List*",
                    "sqs:SetQueueAttributes",
                    "sqs:SetQueueAttributes"
                ]
            }
        ],
        "Version": "2012-10-17"
    }

**Trust Policy**:

Note: Ensure you have the correct source AWS Account ID (that is running Probator) and the Instance Profile or Role Name populated here. ::

    {
    "Version": "2012-10-17",
    "Statement": [
    {
         "Sid": "",
         "Effect": "Allow",
         "Principal": {
         "AWS": [
             "arn:aws:iam::<probator-account-id>:role/<probator-instance-role-name>"
             ],
             "Service": "ec2.amazonaws.com"
         },
         "Action": "sts:AssumeRole"
         }
       ]
      }


3. Building
^^^^^^^^^^^

All the files required to build the image are in the packer subdirectory. Remember to check that your AWS Credentials have been properly exported or the next command will likely hang and time out.

* Execute the following command from the packer directory in the Probator repo to have packer build your custom AMI.::

    packer build -only ami -var-file variables/my-env.json build.json


Assuming your variables are correct and you have the proper AWS permissions, packer should create an AMI. If steps fail, try executing ``packer`` with the ``-debug`` flag and step through the build process to identify where it is breaking. If it succeeds you should see something like this. Ensure you note the AMI ID that was created as you'll use it in the next step ::

    ==> ami: Waiting for the instance to stop...
    ==> ami: Creating the AMI: probator @master 2019-02-24 13:47:30
    **    ami: AMI: ami-xxxxxxxx **
    ==> ami: Waiting for AMI to become ready...
    ==> ami: Adding tags to AMI (ami-xxxxxxxx)...
    ==> ami: Tagging snapshot: snap-xxxxxxxxxx
    ==> ami: Creating AMI tags
        ami: Adding tag: "Accounting": "yourteam.accounting.tag"
        ami: Adding tag: "Name": "Probator System Image"
        ami: Adding tag: "SourceAmi": "ami-005bdb005fb00e791"
        ami: Adding tag: "Owner": "teamname@yourcompany.com"
    ==> ami: Creating snapshot tags
    ==> ami: Terminating the source AWS instance...
    ==> ami: Cleaning up any extra volumes...
    ==> ami: No volumes to clean up, skipping
    ==> ami: Deleting temporary keypair...
    Build 'ami' finished.


4. Launching the AMI
^^^^^^^^^^^^^^^^^^^^

You can now launch this ``AMI`` within the ``EC2`` (Launch Instance) section of your AWS Console. When launching your ``AMI`` ensure the following:

    1. Ensure you use the Instance Profile to launch your instance
    2. Configure your Security Groups should be open on ``22/443`` so that you can connect to both the ``Probator`` UI and the instance itself for troubleshooting.
    3. ssh into the instance and grab the password from ``/var/log/supervisor/probator-api-stdout---supervisor-*****.log``
    4. Connect to https://<yourinstanceip> and login (username :: **admin**)!


You can now add new accounts under the **Accounts** tab in the ``Probator`` UI. Please check out the `User Guide`_ for further details on how to use the UI, review results and update configuration.


.. _`AWS`: https://aws.amazon.com/
.. _`AWS Credentials`: https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html
.. _`Packer`: https://packer.io/downloads.html
.. _`appropriate permissions`: https://www.packer.io/docs/builders/amazon.html#iam-task-or-instance-role
.. _`STS AssumeRole`: https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRole.html
.. _`IAM policy`: https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html
.. _`IAM Role`: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/iam-roles-for-amazon-ec2.html
.. _`AWS Managed ReadOnlyAccess policy`: http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html#aws-managed-policies
.. _`User Guide`: userguide.rst
.. _`additional options`: additional_options.rst
